import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'lista.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: (Colors.white),
      body:
      Center(
          child:
          SingleChildScrollView(
            padding:EdgeInsets.fromLTRB(25.0, 25.0, 25.0,50.0),
            child:
            Center(
                child:  Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.account_box, size: 130.0, color: Colors.pinkAccent[100]),
                    TextField(keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        labelText: ('Usuário'),
                        labelStyle: TextStyle(color: Colors.pinkAccent[100], fontSize: 14.0),
                      ),
                    ),
                    TextField(keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          labelText: ('Senha'),
                          labelStyle: TextStyle(color: Colors.pinkAccent[100], fontSize: 14.0)
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(top: 25.0, bottom: 25.0),
                        child:  Container(
                            height: 30.0,
                            child: RaisedButton(
                              onPressed:(){
                                Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(builder: (context) => Listas() ),
                                );
                              },
                              child: Text('Entrar',
                                style: TextStyle (color: Colors.white, fontSize: 20.0),),
                              color: Colors.pinkAccent[100],

                            )

                        )
                    )
                  ],

                )
            ),




          )
      ),


    );
  }
}
