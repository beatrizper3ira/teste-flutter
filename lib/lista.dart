import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:teste_bp/scheduling.dart';

import 'login.dart';

 class Listas extends StatefulWidget {
   @override
   _ListasState createState() => _ListasState();

 }


 
 class _ListasState extends State<Listas> {
   String nome1 ='JOÃO',prof1='Encanador',tel1='(75)98314-4823';
   String nome2 ='BEATRIZ',prof2='Professora',tel2='(75)98314-4821';
   String nome3 ='JORGE',prof3='Caminhoneiro',tel3='(75)98314-4820';
   @override
   Widget build(BuildContext context) {

     return Scaffold(
         appBar: AppBar(
         backgroundColor:Colors.white,
         title:Text('SERVIÇOS', style: TextStyle(color: Colors.pink, fontSize: 15.0),),
     centerTitle: true,
           actions: [

             Padding(
               padding: EdgeInsets.only(right: 10.0),
               child:PopupMenuButton(
                 child: Icon(Icons.more_vert, size: 20, color: Colors.black,),
                 onSelected: (result){
                 if(result ==0 ){
                   Navigator.of(context).pushReplacement(
                     MaterialPageRoute(builder: (context) => Login() ),
                   );
                 }
               },
                 itemBuilder: (context)=>[
                   PopupMenuItem (
                     value:0,
                     child: Center(
                       child: Icon(Icons.power_settings_new, color: Colors.red),
                     ),
                   ),
                 ],
               ),

             ),

           ],
         ),
       body:
       ListView(
         children: <Widget>[
           GestureDetector(

             onTap:(){
               callView(nome1,prof1,tel1);
             } ,

             child: Card(
               child: Padding(
                 padding: EdgeInsets.all(10.0),
                 child: Row(
                   children: <Widget>[
                     Container(
                       width: 80.0,
                       height: 80.0,
                       decoration: BoxDecoration(
                         shape: BoxShape.circle,
                         image:DecorationImage(
                           image: AssetImage('assets/image.png'),
                           fit: BoxFit.fill,
                         ),
                       ),
                     ),
                      Padding(
                          padding:EdgeInsets.only(left: 10.0)  ),
                   Column(
                     crossAxisAlignment: CrossAxisAlignment.start,
                     mainAxisAlignment: MainAxisAlignment.start,
                     children: <Widget>[
                       Text(nome1,style: TextStyle(fontSize:14.0, color: Colors.pinkAccent, fontWeight: FontWeight.bold ),),
                       Text(prof1,style: TextStyle(fontSize:12.0, color: Colors.black,),),
                       Text(tel1,style: TextStyle(fontSize:12.0, color: Colors.black),),
                     ],
                   ),
                   ],
                 ),
               ),

             ),

           ),

           GestureDetector(
             onTap:(){
               callView(nome2,prof2,tel2);
             } ,
             child: Card(
               child: Padding(
                 padding: EdgeInsets.all(10.0),
                 child: Row(
                   children: <Widget>[
                     Container(
                       width: 80.0,
                       height: 80.0,
                       decoration: BoxDecoration(
                         shape: BoxShape.circle,
                         image:DecorationImage(
                           image: AssetImage('assets/image.png'),
                           fit: BoxFit.fill,
                         ),
                       ),
                     ),
                     Padding(
                         padding:EdgeInsets.only(left: 10.0)  ),
                     Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       mainAxisAlignment: MainAxisAlignment.start,
                       children: <Widget>[
                         Text(nome2,style: TextStyle(fontSize:14.0, color: Colors.pinkAccent, fontWeight: FontWeight.bold ),),
                         Text(prof2,style: TextStyle(fontSize:12.0, color: Colors.black,),),
                         Text(tel2,style: TextStyle(fontSize:12.0, color: Colors.black),),
                       ],
                     ),
                   ],
                 ),
               ),

             ),

           ),

           GestureDetector(
             onTap:(){
               callView(nome3,prof3,tel3);
             } ,
             child: Card(
               child: Padding(
                 padding: EdgeInsets.all(10.0),
                 child: Row(
                   children: <Widget>[
                     Container(
                       width: 80.0,
                       height: 80.0,
                       decoration: BoxDecoration(
                         shape: BoxShape.circle,
                         image:DecorationImage(
                           image: AssetImage('assets/image.png'),
                           fit: BoxFit.fill,
                         ),
                       ),
                     ),
                     Padding(
                         padding:EdgeInsets.only(left: 10.0)  ),
                     Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       mainAxisAlignment: MainAxisAlignment.start,
                       children: <Widget>[
                         Text(nome3,style: TextStyle(fontSize:14.0, color: Colors.pinkAccent, fontWeight: FontWeight.bold ),),
                         Text(prof3,style: TextStyle(fontSize:12.0, color: Colors.black,),),
                         Text(tel3,style: TextStyle(fontSize:12.0, color: Colors.black),),
                       ],
                     ),
                   ],
                 ),
               ),

             ),

           ),


         ],
       ),

     );
   }

   callView(String nome, String prof, String tel ) {
     Navigator.of(context).push(
       MaterialPageRoute(builder: (context) => Agendamento(nome: nome,prof: prof,tel: tel ,) ),
     );
   }
 }

