import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
class Agendamento extends StatefulWidget {



  final String nome, prof, tel;
  Agendamento ({this.nome,this.prof,this.tel});


  @override
  _AgendamentoState createState() => _AgendamentoState();
}

class _AgendamentoState extends State<Agendamento> {
  final _formKey = GlobalKey<FormState>();
  ScrollController _controllerOne = new ScrollController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.black,
          iconSize: 16.0,
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        backgroundColor:Colors.white,
        title:Text('AGENDAMENTO', style: TextStyle(color: Colors.pink, fontSize: 15.0),),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: ()
    {
      if (_formKey.currentState.validate()) {
        _showToast();
        Navigator.of(context).pop();
      }
    else{ print("Preencha todos os campos!");

    }
          },
        child: Icon(Icons.save),
        backgroundColor: Colors.pink,
      ),
      body: SingleChildScrollView(
        controller: _controllerOne,
        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),

        padding: EdgeInsets.all(10.0),
        child: Form(
          key: _formKey,
          child:  Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              GestureDetector(
                child:
                Center(
                  child: Container(
                    width: 150.0,
                    height: 150.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image:DecorationImage(
                        image: AssetImage('assets/image.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
              ),


              Text('NOME: ${widget.nome}', style:TextStyle(color: Colors.pink, fontSize:20.0),),
              Text('PROFISSÃO: ${widget.prof}', style:TextStyle(color: Colors.pink, fontSize:20.0),),
              Text('TELEFONE:${widget.tel}', style:TextStyle(color: Colors.pink, fontSize:20.0),),

              TextFormField(
                validator: (value)=> value!=""?null:"Este campo não pode ficar vazio!",
                decoration: InputDecoration(
                    labelText: ('Data:'),
                    labelStyle: TextStyle(color: Colors.pinkAccent[100], fontSize: 14.0)
                ),
              ),
              TextFormField(
                validator: (value)=> value!=""?null:"Este campo não pode ficar vazio!",
                decoration: InputDecoration(
                    labelText: ('Hora:'),
                    labelStyle: TextStyle(color: Colors.pinkAccent[100], fontSize: 14.0)
                ),
              ),
              TextFormField(
                validator: (value)=> value!=""?null:"Este campo não pode ficar vazio!",
                decoration: InputDecoration(
                    labelText: ('Endereço:'),
                    labelStyle: TextStyle(color: Colors.pinkAccent[100], fontSize: 14.0)
                ),
              ),
              TextFormField(
                validator: (value)=> value!=""?null:"Este campo não pode ficar vazio!",
                decoration: InputDecoration(
                    labelText: ('Tipo de serviço:'),
                    labelStyle: TextStyle(color: Colors.pinkAccent[100], fontSize: 14.0)
                ),
              ),








            ],
          ),
        ),



      ),
    );
  }

  _showToast() => Fluttertoast.showToast(
      msg: "Agendado",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1);}
